name: solarus
version: '1.6.5'
summary: A lightweight, free and open-source game engine for Action-RPGs.
description: |
  Solarus is a free and open source Action-RPG game engine, licensed under GPL, and written in C++.
  It runs games (called *quests*) written in Lua, and can work on a great number of platforms.
  Solarus also features an editor application to help you create your own quests.

  A collection of Solarus Quests can be explored in the Solarus website:
  * https://www.solarus-games.org/games/

  Information about Solarus and making quests can be found in the Solarus communities:
  * Discord: https://discord.gg/yYHjJHt
  * Forum: https://forum.solarus-games.org/
contact: https://solarus-games.org/about/contact/
donation: https://solarus-games.org/about/donate/
website: https://www.solarus-games.org
source-code: https://gitlab.com/solarus-games/solarus-snap.git
issues: https://gitlab.com/solarus-games/solarus-snap/-/issues
license: GPL-3.0

base: core22
confinement: strict
grade: stable

architectures:
  - build-on: amd64
    build-for: amd64

apps:
  run:
    command: usr/bin/solarus-run
    extensions: [kde-neon]
    environment:
      DISABLE_WAYLAND: 1
    plugs:
      - audio-playback
      - home
      - joystick
      - network
      - network-bind
      - removable-media
      - unity7
  launcher:
    command: usr/bin/solarus-launcher
    desktop: usr/share/applications/org.solarus_games.solarus.Launcher.desktop
    extensions: [kde-neon]
    environment:
      DISABLE_WAYLAND: 1
    plugs:
      - audio-playback
      - home
      - joystick
      - network
      - network-bind
      - removable-media
      - unity7
  quest-editor:
    command: usr/bin/solarus-quest-editor
    desktop: usr/share/applications/solarus-quest-editor.desktop
    extensions: [kde-neon]
    environment:
      DISABLE_WAYLAND: 1
    plugs:
      - audio-playback
      - home
      - joystick
      - network
      - network-bind
      - removable-media
      - unity7

parts:
  solarus:
    plugin: cmake
    cmake-parameters:
      - "-DCMAKE_INSTALL_PREFIX=/usr"
      - "-DSOLARUS_TESTS=OFF"
    source-type: git
    source: https://gitlab.com/solarus-games/solarus.git
    source-tag: v1.6.5
    build-snaps:
      - kf5-5-113-qt-5-15-11-core22-sdk
    build-packages:
      - build-essential
      - libglm-dev
      - libluajit-5.1-dev
      - libmodplug-dev
      - libopenal-dev
      - libphysfs-dev
      - libsdl2-dev
      - libsdl2-image-dev
      - libsdl2-ttf-dev
      - libvorbis-dev
    stage-packages:
      - libluajit-5.1-2
      - libmodplug1
      - libopenal1
      - libphysfs1
      - libsdl2-2.0-0
      - libsdl2-image-2.0-0
      - libsdl2-ttf-2.0-0
      - libvorbis0a
      - libvorbisfile3
      - libxinerama1
      - libxrandr2
    override-pull: |
      craftctl default
      git -C "$CRAFT_PART_SRC" apply \
        "$CRAFT_PROJECT_DIR"/snap/local/patches/sl-v1.6.5-no-native-dialog.patch
    override-prime: |
      craftctl default
      sed --in-place \
        's|^Icon=.*|Icon=${SNAP}/usr/share/icons/hicolor/scalable/apps/org.solarus_games.solarus.Launcher.svg|' \
        usr/share/applications/org.solarus_games.solarus.Launcher.desktop
  solarus-quest-editor:
    after: [solarus]
    plugin: cmake
    cmake-parameters:
      - "-DCMAKE_INSTALL_PREFIX=/usr"
      - "-DCMAKE_FIND_ROOT_PATH=$CRAFT_STAGE\\;'$CMAKE_FIND_ROOT_PATH'"
    source-type: git
    source: https://gitlab.com/solarus-games/solarus-quest-editor.git
    source-tag: v1.6.5
    build-snaps:
      - kf5-5-113-qt-5-15-11-core22-sdk
    build-packages:
      - build-essential
      - libglm-dev
      - libluajit-5.1-dev
      - libmodplug-dev
      - libopenal-dev
      - libphysfs-dev
      - libsdl2-dev
      - libsdl2-image-dev
      - libsdl2-ttf-dev
      - libvorbis-dev
    stage-packages:
      - libluajit-5.1-2
      - libmodplug1
      - libopenal1
      - libphysfs1
      - libsdl2-2.0-0
      - libsdl2-image-2.0-0
      - libsdl2-ttf-2.0-0
      - libvorbis0a
      - libvorbisfile3
      - libxinerama1
      - libxrandr2
    override-pull: |
      craftctl default
      git -C "$CRAFT_PART_SRC" apply \
        "$CRAFT_PROJECT_DIR"/snap/local/patches/sqe-v1.6.5-fix-assets-search.patch
      git -C "$CRAFT_PART_SRC" apply \
        "$CRAFT_PROJECT_DIR"/snap/local/patches/sqe-v1.6.5-no-native-dialog.patch
    override-prime: |
      craftctl default
      sed --in-place \
        's|^Icon=.*|Icon=${SNAP}/usr/share/icons/hicolor/scalable/apps/solarus-quest-editor.svg|' \
        usr/share/applications/solarus-quest-editor.desktop

layout:
  /usr/share/solarus-gui:
    symlink: $SNAP/usr/share/solarus-gui
  /usr/share/solarus-quest-editor:
    symlink: $SNAP/usr/share/solarus-quest-editor

lint:
  ignore:
    - library:
        - usr/lib/**/libgraphite2.so*
        - usr/lib/**/libharfbuzz.so*
        - usr/lib/**/libpulse-simple.so*
