# Solarus Snap

This repository contains the source files for building the
[Solarus Snap](https://snapcraft.io/solarus) package.

## Installation

To manage Snaps, first ensure that the
[Snap daemon](https://snapcraft.io/docs/installing-snapd) is installed in your system.

To install the latest `stable` Solarus Snap from the Snap Store:
```
sudo snap install solarus
```

To install the latest `edge` (development) Solarus Snap from the Snap Store:
```
sudo snap install solarus --edge
```

To switch and/or update an already installed Solarus Snap from the Snap Store:
```
sudo snap refresh solarus --stable  # switch to and/or update 'stable'
sudo snap refresh solarus --edge    # switch to and/or update 'edge'
```

The Solarus Snap can be connected to the following optional
[interfaces](https://snapcraft.io/docs/supported-interfaces):

* `removable-media`: To read/write files on removable storage devices.

To connect any of the above interfaces to the Solarus Snap after installation:
```
sudo snap connect solarus:removable-media
```

## Usage

After the Solarus Snap is [installed](#installation), the Solarus Launcher and the Solarus Quest
Editor will be available in the desktop applications menu of the system.

In addition, the following command line programs are also available:

* `solarus.run /path/to/quest`: Starts a Solarus Quest (game). The `quest` can be a
  packaged `.solarus` file or an unpacked Quest directory.
* `solarus.launcher`: Starts the Solarus Launcher graphical interface (for players).
* `solarus.quest-editor`: Starts the Solarus Quest Editor graphical interface (for developers).

## Local Build

To build Snaps locally, first ensure that
[Snapcraft](https://snapcraft.io/docs/snapcraft-setup) is installed in your system.

To build the Solarus Snap locally:
```
snapcraft --use-lxd
```

To install the built Solarus Snap locally:
```
sudo snap install solarus_*.snap --dangerous
```

Reference documentation:
* <https://snapcraft.io/docs/snapcraft-quickstart>

## Manual Publish

To manually publish a [locally built](#local-build) Solarus Snap, first login to the Snap Store:
```
snapcraft login
```

To push the built Solarus Snap to the `edge` (development) channel:
```
snapcraft push --release=edge solarus_*.snap
```
> **Note:** The above command will report the revision number of the pushed Snap.

To promote a pushed Solarus Snap to the `stable` channel:
```
snapcraft release solarus REVISION stable
```

Reference documentation:
* <https://snapcraft.io/docs/releasing-your-app>
* <https://snapcraft.io/docs/publish-to-a-branch>
